var time = [];
var tension;

$.ajax({
  url: '/json',
  dataType: 'json',
  async: false,
  success: function(result) {
    tension = result.tension;
    $.each(result.timestamp, function(index, value) {
      var date = new Date(value * 1000);
      var year = date.getFullYear();
      var month = date.getMonth() + 1;
      var day = date.getDate();
      var hour = date.getHours();
      var min = date.getMinutes();
      var sec = date.getSeconds();

      month = (month < 10 ? "0" : "") + month;
      day = (day < 10 ? "0" : "") + day;
      hour = (hour < 10 ? "0" : "") + hour;
      min = (min < 10 ? "0" : "") + min;
      sec = (sec < 10 ? "0" : "") + sec;
      var str = day + "/" + month + "/" + year + " " + hour + ":" + min + ":" + sec;
      time[index] = str;
    });
  }
});

Chart.defaults.global.legend.display = false;

var config = {
  type: 'line',
  data: {
    labels: time,
    datasets: [{
      data: tension,
      borderColor: "#cc0000",
      fill: false
    }]
  },
  options: {
    responsive: true,
    title: {
      display: true,
      text: "Temperature"
    },
    pan: {
      enabled: true,
      sensitivity: 0.1,
      mode: 'xy'
    },
    zoom: {
      enabled: true,
      //drag: true,
      sensitivity: 1,
      mode: 'x'
    }
  }
};

var ctx = document.getElementById("chart").getContext("2d");
var chart = new Chart(ctx, config);

function resetZoom() {
  chart.resetZoom();
};
