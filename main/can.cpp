#include <Arduino.h>
#include "HardwareSerial.h"
#include "can.h"
#include "pins.h"

extern HardwareSerial can;

ADS122U04::ADS122U04()
{
        NewDataAvailable = false;
}

ADS122U04::ADS122U04(uint8_t m_drdy) : drdy(m_drdy)
{
        NewDataAvailable = false;
}

void ADS122U04::begin(void)
{
        pinMode(drdy, INPUT);
        Config_Reg0 = 0x08;
        Config_Reg1 = 0x00;
        Config_Reg2 = 0x00;
        Config_Reg3 = 0x00;
        Config_Reg4 = 0x48;
        writeReg(CONFIG_REG0_ADDRESS, Config_Reg0);
        delay(50);
        writeReg(CONFIG_REG1_ADDRESS, Config_Reg1);
        delay(50);
        writeReg(CONFIG_REG2_ADDRESS, Config_Reg2);
        delay(50);
        writeReg(CONFIG_REG3_ADDRESS, Config_Reg3);
        delay(50);
        writeReg(CONFIG_REG4_ADDRESS, Config_Reg4);
        delay(100);
        Config_Reg0 = readReg(CONFIG_REG0_ADDRESS);
        Config_Reg1 = readReg(CONFIG_REG1_ADDRESS);
        Config_Reg2 = readReg(CONFIG_REG2_ADDRESS);
        Config_Reg3 = readReg(CONFIG_REG3_ADDRESS);
        Config_Reg4 = readReg(CONFIG_REG4_ADDRESS);
        DEBUG_PRINTLN("Config :");
        DEBUG_PRINTLNX(Config_Reg0, HEX);
        DEBUG_PRINTLNX(Config_Reg1, HEX);
        DEBUG_PRINTLNX(Config_Reg2, HEX);
        DEBUG_PRINTLNX(Config_Reg3, HEX);
        DEBUG_PRINTLNX(Config_Reg4, HEX);
}

void ADS122U04::can_write(uint8_t valeur)
{
        can.write(SYNC);
        can.write(valeur);
}

void ADS122U04::writeReg(uint8_t registre, uint8_t valeur)
{
        if(registre > 4)
        {
                DEBUG_PRINTLN("Registre inconnu");
        }
        can.write(SYNC);
        can.write(WREG|(registre << 1));
        can.write(valeur);
}

uint8_t ADS122U04::readReg(uint8_t registre)
{
        uint8_t data;
        if(registre > 4)
        {
                DEBUG_PRINTLN("Registre inconnu");
        }
        can.write(SYNC);
        can.write(RREG|(registre << 1));
        while(can.available() < 1);
        data = (uint8_t)can.read();
        return data;
}


void ADS122U04::reset(void)
{
        can_write(RESET);
}

void ADS122U04::startSync(void)
{
        can_write(START);
}

void ADS122U04::powerDown(void)
{
        can_write(POWERDOWN);
}

void ADS122U04::PGA_ON(void)
{
        Config_Reg0 &= ~_UN(0);
        writeReg(CONFIG_REG0_ADDRESS, Config_Reg0);
}

void ADS122U04::PGA_OFF(void)
{
        Config_Reg0 |= _UN(0);
        writeReg(CONFIG_REG0_ADDRESS,Config_Reg0);
}

void ADS122U04::temperatureSensorON(void)
{
        Config_Reg1 |= _UN(0);
        writeReg(CONFIG_REG1_ADDRESS, Config_Reg1);
}

void ADS122U04::temperatureSensorOFF(void)
{
        Config_Reg1 &= ~_UN(0);
        writeReg(CONFIG_REG1_ADDRESS, Config_Reg1);
}

void ADS122U04::continuousConversionModeON(void)
{
        Config_Reg1 |= _UN(3);
        writeReg(CONFIG_REG1_ADDRESS, Config_Reg1);
}

void ADS122U04::singleShotModeON(void)
{
        Config_Reg1 &= ~_UN(3);
        writeReg(CONFIG_REG1_ADDRESS, Config_Reg1);
}

void ADS122U04::normalModeON(void)
{
        Config_Reg1 &= ~_UN(4);
        writeReg(CONFIG_REG1_ADDRESS, Config_Reg1);
}

void ADS122U04::turboModeON(void)
{
        Config_Reg1 |= _UN(4);
        writeReg(CONFIG_REG1_ADDRESS, Config_Reg1);
}

void ADS122U04::gpio2ControlON(void)
{
        Config_Reg4 &= ~_UN(3);
        writeReg(CONFIG_REG4_ADDRESS, Config_Reg4);
}

void ADS122U04::drdyControlON(void)
{
        Config_Reg4 |= _UN(3);
        writeReg(CONFIG_REG4_ADDRESS, Config_Reg4);
}

void ADS122U04::setGpio(int gpio, int io)
{
        switch(gpio)
        {
        case (GPIO0):
                if(io == INPUT)
                {
                        Config_Reg4 &= ~_UN(4);
                }
                else if(io == OUTPUT)
                {
                        Config_Reg4 |= _UN(4);
                }
                else
                {
                        DEBUG_PRINTLN("input ou output seulement");
                }
                break;

        case (GPIO1):
                if(io == INPUT)
                {
                        Config_Reg4 &= ~_UN(5);
                }
                else if(io == OUTPUT)
                {
                        Config_Reg4 |= _UN(5);
                }
                else
                {
                        DEBUG_PRINTLN("input ou output seulement");
                }
                break;

        case (GPIO2):
                if(io == INPUT)
                {
                        Config_Reg4 &= ~_UN(6);
                }
                else if(io == OUTPUT)
                {
                        Config_Reg4 |= _UN(6);
                }
                else
                {
                        DEBUG_PRINTLN("input ou output seulement");
                }
                break;
        default:
                DEBUG_PRINTLN("Erreur selection GPIO");
        }
        writeReg(CONFIG_REG4_ADDRESS, Config_Reg4);
}

void ADS122U04::setInputMultiplexer(int setting)
{
        if((setting == AIN0_AVSS_CONFIG)||(setting == AIN1_AVSS_CONFIG)||(setting == AIN2_AVSS_CONFIG)||(setting == AIN3_AVSS_CONFIG))
        {
                PGA_OFF();
        }

        Config_Reg0 &= ~INPUT_MULTIPLEXER_MASK;
        Config_Reg0 |= setting;
        writeReg(CONFIG_REG0_ADDRESS, Config_Reg0);
}

void ADS122U04::setDataRate(int datarate)
{
        Config_Reg1 &= ~REG_CONFIG_DR_MASK;

        switch(datarate)
        {
        case (DR_20SPS):
                Config_Reg1 |= REG_CONFIG_DR_20SPS;
                break;
        case (DR_45SPS):
                Config_Reg1 |= REG_CONFIG_DR_45SPS;
                break;
        case (DR_90SPS):
                Config_Reg1 |= REG_CONFIG_DR_90SPS;
                break;
        case (DR_175SPS):
                Config_Reg1 |= REG_CONFIG_DR_175SPS;
                break;
        case (DR_330SPS):
                Config_Reg1 |= REG_CONFIG_DR_330SPS;
                break;
        case (DR_600SPS):
                Config_Reg1 |= REG_CONFIG_DR_600SPS;
                break;
        case (DR_1000SPS):
                Config_Reg1 |= REG_CONFIG_DR_1000SPS;
                break;
        }

        writeReg(CONFIG_REG1_ADDRESS, Config_Reg1);
}

void ADS122U04::setPgaGain(int pgagain)
{
        Config_Reg0 &= ~REG_CONFIG_PGA_GAIN_MASK;

        switch(pgagain)
        {
        case (PGA_GAIN_1):
                Config_Reg0 |= REG_CONFIG_PGA_GAIN_1;
                break;
        case (PGA_GAIN_2):
                Config_Reg0 |= REG_CONFIG_PGA_GAIN_2;
                break;
        case (PGA_GAIN_4):
                Config_Reg0 |= REG_CONFIG_PGA_GAIN_4;
                break;
        case (PGA_GAIN_8):
                Config_Reg0 |= REG_CONFIG_PGA_GAIN_8;
                break;
        case (PGA_GAIN_16):
                Config_Reg0 |= REG_CONFIG_PGA_GAIN_16;
                break;
        case (PGA_GAIN_32):
                Config_Reg0 |= REG_CONFIG_PGA_GAIN_32;
                break;
        case (PGA_GAIN_64):
                Config_Reg0 |= REG_CONFIG_PGA_GAIN_64;
                break;
        case (PGA_GAIN_128):
                Config_Reg0 |= REG_CONFIG_PGA_GAIN_128;
                break;
        }

        writeReg(CONFIG_REG0_ADDRESS,Config_Reg0);
}

uint8_t * ADS122U04::getConfigReg(void)
{
        static uint8_t config_Buff[5];

        Config_Reg0 = readReg(CONFIG_REG0_ADDRESS);
        Config_Reg1 = readReg(CONFIG_REG1_ADDRESS);
        Config_Reg2 = readReg(CONFIG_REG2_ADDRESS);
        Config_Reg3 = readReg(CONFIG_REG3_ADDRESS);
        Config_Reg4 = readReg(CONFIG_REG4_ADDRESS);

        config_Buff[0] = Config_Reg0;
        config_Buff[1] = Config_Reg1;
        config_Buff[2] = Config_Reg2;
        config_Buff[3] = Config_Reg3;
        config_Buff[4] = Config_Reg4;

        return config_Buff;
}

uint32_t ADS122U04::manualRead(void)
{
        static uint8_t UART_Buff[3];
        while(digitalRead(drdy));
        if(!digitalRead(drdy))
        {
                can_write(RDATA);
                while(can.available()<3);
                for (int i = 0; i < 3; i++)
                {
                        UART_Buff[i] = can.read();
                }
                NewDataAvailable = true;
        }

        return (UART_Buff[2]<<16) | (UART_Buff[1]<<8) | (UART_Buff[0]);
}

float ADS122U04::manualReadVolt(void)
{
        uint32_t in = manualRead();
        float out = in * 2.048 / 8388608.0;
        return out;
}

uint32_t ADS122U04::autoRead(void)
{
        static uint8_t UART_Buff[3];
        while(!digitalRead(drdy));
        while(can.available()<3);
        for (int i = 0; i < 3; i++)
        {
                UART_Buff[i] = can.read();
        }
        NewDataAvailable = true;

        return (UART_Buff[2]<<16) | (UART_Buff[1]<<8) | (UART_Buff[0]);
}

float ADS122U04::autoReadVolt(void)
{
        uint32_t in = autoRead();
        float out = in * 2.048 / 8388608.0;
        return out;
}

float ADS122U04::readTemp(void)
{
        static uint8_t UART_Buff[3];
        static float ret;

        if(!digitalRead(drdy))
        {
                can_write(RDATA);
                while(can.available()<3);
                for (int i = 0; i < 3; i++)
                {
                        UART_Buff[i] = can.read();
                }
                NewDataAvailable = true;

                uint32_t assemble = (UART_Buff[2]<<16) | (UART_Buff[1]<<8) | (UART_Buff[0]);
                uint16_t msb = assemble>>10;
                if((msb & 0x2000) != 0)
                {
                        msb--;
                        msb = ~msb;
                        ret = msb * (-0.03125);
                }
                else
                {
                        ret = msb * 0.03125;
                }
        }
        return ret;
}
