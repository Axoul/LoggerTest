#ifndef CONFIG_H
#define CONFIG_H

#define SLEEP_TIME 10 //secondes

#define NBDATASD 1 //Enregistrement sur carte SD toutes les X acquisitions

#define LEDRATE 3 //Periode en sec du flash lumineux

#define ADC 0 //ADS122U04 0 = inactif  1 = actif

#define ANEMOMETRE 0 //0 = Anémometre CV7   1 = Anémometre GILL

#define RESISTANCE0 100.0
#define RRESISTANCE1 3300.0

#define KA 0.905f //SONDE AVEC FILTRE
#define KB -0.871f

#endif
