#include <Arduino.h>
#include <HardwareSerial.h>
#include <time.h>
#include <sys/time.h>
 #include "soc/soc.h"

#include "esp32/ulp.h"
#include "soc/rtc_cntl_reg.h"
#include "driver/rtc_io.h"
#include "driver/adc.h"

#include "pins.h"
#include "sd_fonc.h"
#include "server.h"
#include "can.h"
#include "config.h"

carteSd sd1(5, VSPI);
carteSd sd2(15, HSPI);
HardwareSerial can(1);
ADS122U04 conv(35);

etat_t etatCourant;
RTC_DATA_ATTR static int bootCount = 0;
RTC_DATA_ATTR int fileSd1, fileSd2;
RTC_DATA_ATTR uint8_t sleepTime = 30;
RTC_DATA_ATTR float sauvData[NBDATASD-1];
RTC_DATA_ATTR time_t times[NBDATASD-1];

time_t debut, before, now;
extern time_t rtc;

static void deepSleep(uint8_t sec);
void lightSleep(uint8_t sec);
void ledFlash(void);
void initCan(void);

float data = 0;

bool stopTimerMain = false;
extern volatile bool stopTimer;
extern portMUX_TYPE timerMux;

void setup()
{
        WRITE_PERI_REG(RTC_CNTL_BROWN_OUT_REG, 0);
        debut = time(&rtc);
        ++bootCount;
        esp_sleep_wakeup_cause_t restart = esp_sleep_get_wakeup_cause();
        pc.begin(115200);
        can.begin(9600, SERIAL_8N1, 4, 2);
        pinMode(led, OUTPUT);
        pinMode(regSd, OUTPUT);
        pinMode(regCan, OUTPUT);
        pinMode(comWind, OUTPUT);
        pinMode(5, OUTPUT);
        pinMode(15, OUTPUT);
        pinMode(bouton, INPUT_PULLUP);
        analogSetWidth(10);
        analogSetAttenuation(ADC_0db);
        analogSetPinAttenuation(vBat, ADC_6db);
        pc.println(restart);
        if(restart == ESP_SLEEP_WAKEUP_TIMER)//Restart a cause du timer
        {
                rtc_gpio_hold_dis(GPIO_NUM_26);
                rtc_gpio_hold_dis(GPIO_NUM_27);
                sd1.numFile = fileSd1;
                sd2.numFile = fileSd2;
                if(hallRead()>23)
                {
                        SDtoSPIFFS(sd1.lastFile());
                        monitor_Wifi();
                        etatCourant = MONITOR;
                }
                else
                {
                        etatCourant = LECTURE;
                }
        }
        else if(restart == ESP_SLEEP_WAKEUP_ULP)
        {
                pc.println("ULP");
        }
        else
        {
                digitalWrite(regSd, HIGH);
                delay(50);
                fileSd1 = sd1.init("time, temperature");
                delay(50);
                fileSd2 = sd2.init("time, temperature");
                //sd1.genJson();
                digitalWrite(regSd, LOW);
                etatCourant = INIT;
        }
}


void loop()
{
        float temp;
        now = time(&rtc);
        switch(etatCourant)
        {
        case INIT:
                wifi_Init();
                //pc.println("Allumage CAN");
                alarm();
                etatCourant = INIT_WIFI;
                //pc.println(1.6f*analogRead(vBat)/1023.0f);
                break;

        case INIT_WIFI:
                portENTER_CRITICAL_ISR(&timerMux);
                stopTimerMain = stopTimer;
                portEXIT_CRITICAL_ISR(&timerMux);
                if(!digitalRead(bouton))
                {
                        pc.println("appui Off");
                        while(!digitalRead(bouton));
                        stopWifi();
                        etatCourant = LECTURE;
                }
                if(stopTimerMain)
                {
                        stopWifi();
                        stopTimerMain = false;
                        portENTER_CRITICAL_ISR(&timerMux);
                        stopTimer = false;
                        portEXIT_CRITICAL_ISR(&timerMux);
                        etatCourant = LECTURE;
                }
                break;

        case LECTURE:
                if(hallRead()>23)
                {
                        pc.println("c'est bon !!");
                }
                pc.println("LECTURE");
                delay(4000);
                etatCourant = ECRITURE;
                break;

        case ECRITURE:
                pc.println("ECRITURE");
                delay(2000);
                etatCourant = DODO;
                break;

        case MONITOR:
                if(!digitalRead(bouton))
                {
                        pc.println("appui Off");
                        while(!digitalRead(bouton));
                        stopWifi();
                        etatCourant = LECTURE;
                }
                break;

        case DODO:
                //delay(2000);
                //etatCourant = LECTURE;
                deepSleep(sleepTime);
                break;

        default:
                pc.println("Erreur Machine a etat !");
        }
        before = now;
        //pc.println(etatCourant);
        delay(10);
}

static void deepSleep(uint8_t sec)
{
        digitalWrite(regSd, LOW);
        digitalWrite(regCan, LOW);
        digitalWrite(comWind, LOW);
        rtc_gpio_hold_en(GPIO_NUM_26);
        rtc_gpio_hold_en(GPIO_NUM_27);
        rtc_gpio_hold_en(GPIO_NUM_21);
        time_t diff = time(&rtc) - debut;
        rtc_gpio_pullup_en(GPIO_NUM_22);

        adc1_config_channel_atten(ADC1_CHANNEL_0, ADC_ATTEN_DB_11);
        adc1_config_width(ADC_WIDTH_BIT_10);
        adc1_ulp_enable();
        rtc_gpio_init(GPIO_NUM_36);
        const ulp_insn_t program[] = {
                I_DELAY(32000),    // Wait until ESP32 goes to deep sleep
                M_LABEL(1),        // LABEL 1
                I_MOVI(R0, 0),       // Set reg. R0 to initial 0
                I_MOVI(R2, 0),       // Set reg. R2 to initial 0
                M_LABEL(2),        // LABEL 2
                I_ADDI(R0, R0, 1),   // Increment cycle counter (reg. R0)
                I_ADC(R1, 0, 0),     // Read ADC value to reg. R1
                I_ADDR(R2, R2, R1),   // Add ADC value from reg R1 to reg. R2
                M_BL(2, 4),        // If cycle counter is less than 4, go to LABEL 2
                I_RSHI(R0, R2, 2), // Divide accumulated ADC value in reg. R2 by 4 and save it to reg. R0
                M_BGE(3, 320), // If average ADC value from reg. R0 is higher or equal than high_adc_treshold, go to LABEL 3
                M_BL(3, 0), // If average ADC value from reg. R0 is lower than low_adc_treshold, go to LABEL 3
                M_BX(1),           // Go to LABEL 1
                M_LABEL(3),        // LABEL 3
                I_WAKE(),          // Wake up ESP32
                I_END(),           // Stop ULP program timer
                I_HALT()           // Halt the coprocessor
        };
        size_t load_addr = 0;
        size_t size = sizeof(program)/sizeof(ulp_insn_t);
        ulp_process_macros_and_load(load_addr, program, &size);
        ulp_run(load_addr);

        esp_sleep_enable_timer_wakeup((sec-diff)*1000000);
        esp_sleep_enable_ulp_wakeup();

        esp_deep_sleep_start();
}

void lightSleep(uint8_t sec)
{
        esp_sleep_enable_timer_wakeup(sec*1000000);
        esp_light_sleep_start();
}

void ledFlash(void)
{
        static uint8_t periode = 0;
        if(before != now)
        {
                if(periode == LEDRATE-1)
                {
                        digitalWrite(led, HIGH);
                        delay(20);
                        digitalWrite(led, LOW);
                        periode = 0;
                }
                else
                {
                        periode++;
                }
        }
}

void initCan(void)
{
        digitalWrite(regCan, HIGH);
        delay(50);
        conv.reset();
        delay(50);
        conv.begin();
        //conv.PGA_OFF();
        //conv.setDataRate(DR_20SPS);
        conv.PGA_ON();
        conv.setPgaGain(PGA_GAIN_8);
        //conv.setInputMultiplexer(AIN0_AIN1_CONFIG);
        //conv.temperatureSensorON();
        //conv.drdyControlON();
        //conv.setGpio(GPIO2, OUTPUT);
        //conv.continuousConversionModeON();
        conv.singleShotModeON();
        conv.powerDown();
}
