#ifndef PINS_H
#define PINS_H

#define pc Serial

#define DEBUG 1

const uint8_t led = 25;
const uint8_t vBat = 34;
const uint8_t regSd = 26;
const uint8_t regCan = 27;
const uint8_t comWind = 21;
const uint8_t bouton = 22;

enum etat_t {
        INIT,
        INIT_WIFI,
        LECTURE,
        ECRITURE,
        MONITOR,
        DODO
};

#endif
