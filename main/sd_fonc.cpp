#include <Arduino.h>
#include <stdio.h>
#include <stdarg.h>
#include <ArduinoJson.h>
#include "FS.h"
#include "SD.h"
#include "SPI.h"
#include "SPIFFS.h"
#include "sd_fonc.h"
#include "config.h"

extern time_t now;

carteSd::carteSd(uint8_t m_ssPin, SPIClass spi) : ssPin(m_ssPin), spi_bus(spi) {}

int carteSd::init(const char * title)
{
        if (!SD.begin(ssPin, spi_bus))
        {
                DEBUG_PRINTLN("Carte non presente -- INIT");
                return 0;
        }
        DEBUG_PRINTLN("Carte Initialise");
        int n = 1;
        sprintf(filename, "/logger%02d.csv", n);
        while(SD.exists(filename))
        {
                n++;
                sprintf(filename, "/logger%02d.csv", n);
        }
        //delay(500);
        File dataFile = SD.open(filename, FILE_WRITE);
        if(dataFile)
        {
                dataFile.println(title);
                dataFile.close();
                DEBUG_PRINT("Ecriture reussie sur : ");
                DEBUG_PRINTLN(filename);
                SD.end();
        }
        else
        {
                DEBUG_PRINT("Ecriture impossible sur : ");
                DEBUG_PRINTLN(filename);
        }
        numFile = n;
        return n;
}

void carteSd::logger(unsigned long timestamp, int n_args, ...)
{
        va_list ap;
        va_start(ap, n_args);

        if (!SD.begin(ssPin, spi_bus))
        {
                DEBUG_PRINTLN("Carte non presente -- LOGGER");
                return;
        }
        sprintf(filename, "/logger%02d.csv", numFile);

        File dataFile = SD.open(filename, FILE_WRITE);
        if(dataFile)
        {
                char buf[50];
                double in[n_args];

                for(int i=0; i<n_args; i++)
                {
                        in[i] = va_arg(ap, double);
                }

                switch(n_args)
                {
                case 1:
                        sprintf(buf, "%lu, %.3f", timestamp, in[0]);
                        break;

                case 2:
                        sprintf(buf, "%lu, %.3f, %.3f", timestamp, in[0], in[1]);
                        break;

                case 3:
                        sprintf(buf, "%lu, %.3f, %.3f, %.3f", timestamp, in[0], in[1], in[2]);
                        break;

                case 4:
                        sprintf(buf, "%lu, %.3f, %.3f, %.3f, %.3f", timestamp, in[0], in[1], in[2], in[3]);
                        break;
                }

                dataFile.println(buf);
                dataFile.close();
                SD.end();
        }
        else
        {
                DEBUG_PRINT("Ecriture impossible sur : ");
                DEBUG_PRINTLN(filename);
                SD.end();
        }

        va_end(ap);
        nbData = n_args+1;
}

char * carteSd::lastFile(void)
{
        sprintf(filenameFind, "/logger%02d.csv", numFile);
        return filenameFind;
}

void carteSd::genJson(void)
{
        static char inData[100];
        static uint32_t index = 0;

        double data[2];
        const uint8_t bytesPerLine = 19;
        bool firstLine = 1;

        if (!SD.begin(ssPin, spi_bus))
        {
                DEBUG_PRINTLN("Carte non presente");
                return;
        }
        if(!SPIFFS.begin(true))
        {
                DEBUG_PRINTLN("Erreur Init SPIFFS !");
                return;
        }
        if(SPIFFS.exists("/data.json"))
        {
                SPIFFS.remove("/data.json");
        }
        File dataFile = SD.open("LOGGER06.CSV");
        fs::File jsonFile = SPIFFS.open("/data.json", "w");

        uint32_t lines = (dataFile.size()/bytesPerLine)-1;

        const size_t bufferSize = nbData*JSON_ARRAY_SIZE(lines) + JSON_OBJECT_SIZE(nbData);

        DEBUG_PRINTLN(lines);
        DEBUG_PRINTLN(bufferSize);

        DynamicJsonBuffer jsonBuffer(bufferSize);

        JsonObject& root = jsonBuffer.createObject();
        JsonArray& label0 = root.createNestedArray("tension");
        JsonArray& label1 = root.createNestedArray("timestamp");

        dataFile.seek(0);
        if(dataFile)
        {
                while(dataFile.available())
                {
                        char in = dataFile.read();
                        if(in == '\n')
                        {
                                if(!firstLine)
                                {
                                        sscanf(inData, "%lf, %lf", &data[0], &data[1]);
                                        label0.add(data[0]);
                                        label1.add((long)data[1]);
                                        data[0] = 0;
                                        data[1] = 0;
                                }
                                else
                                {
                                        firstLine = 0;
                                }
                                index = 0;
                                inData[index] = 0;

                        }
                        else
                        {
                                inData[index] = in;
                                index++;
                                inData[index] = '\0';
                        }
                }
        }
        dataFile.close();
        root.printTo(jsonFile);
        jsonFile.close();
}
