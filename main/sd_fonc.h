#ifndef SD_FONC_H
#define SD_FONC_H
#include "SPI.h"
#define DEBUG 1

#if DEBUG
 #define DEBUG_PRINT(x) Serial.print(x)
 #define DEBUG_PRINTX(x,y) Serial.print(x,y)
 #define DEBUG_PRINTLN(x) Serial.println(x)
 #define DEBUG_PRINTLNX(x,y) Serial.println(x,y)
#else
 #define DEBUG_PRINT(x)
 #define DEBUG_PRINTX(x,y)
 #define DEBUG_PRINTLN(x)
 #define DEBUG_PRINTLNX(x,y)
#endif

class carteSd
{
public:

  int numFile;

  carteSd(uint8_t m_ssPin, SPIClass spi);
  int init(const char * title);
  void logger(unsigned long timestamp, int n_args, ...);
  char * lastFile(void);
  void genJson(void);


private:

  char filename[16];
  uint8_t ssPin;
  SPIClass spi_bus;
  char filenameFind[16];
  uint8_t nbData;
};

#endif
