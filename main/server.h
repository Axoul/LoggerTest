#ifndef SERVER_H
#define SERVER_H

#define DEBUG 1

#ifdef DEBUG
 #define DEBUG_PRINT(x) Serial.print(x)
 #define DEBUG_PRINTX(x,y) Serial.print(x,y)
 #define DEBUG_PRINTLN(x) Serial.println(x)
 #define DEBUG_PRINTLNX(x,y) Serial.println(x,y)
#else
 #define DEBUG_PRINT(x)
 #define DEBUG_PRINTX(x,y)
 #define DEBUG_PRINTLN(x)
 #define DEBUG_PRINTLNX(x,y)
#endif


#define TZ 2
#define DST_MN 60
#define TZ_MN ((TZ)*60)


void wifi_Init(void);
void monitor_Wifi(void);
void stopWifi(void);
void sendDataWs(float data1, unsigned long data2, char * unit);
void SDtoSPIFFS(char * fileToConvert);
void alarm(void);
void stopAlarm(void);


#endif
